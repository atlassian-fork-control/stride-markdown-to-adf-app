/**
 * This module handles the lifecycle routing for our example app.
 * This includes the install, uninstall, and descriptor endpoints.
 */

var express = require('express');
var router = express.Router();
const fs = require('fs');
const path = require('path');
var messages = require('../helper/messages');


/* GET home page. */
router.get('/', function (req, res) {
  res.render('index', { title: 'Express' })
})

router.get('/descriptor', (req, res) => {
  let descriptor = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../app-descriptor.json')).toString()
  )
  descriptor.baseUrl = 'https://' + req.headers.host
  res.contentType = 'application/json'
  res.send(descriptor)
  res.end()
})

router.post('/installed', (req, res, next) => {
  // context.cloudId        - the site id that contains the conversation that the app was installed in.
  // context.userId         - the user id that installed the app.
  // context.conversationId - the conversation id that the app was installed in.
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }
  const message = {
  
    version: 1,
    type: "doc",
    content: [
      {
        type: "paragraph",
        content: [
          {
            type: "text",
            text: "Hi there! Thanks for adding me to this conversation. To see me in action, just mention me in a message."
          }
        ]
      }
    ]
}
messages.sendMessage(context.cloudId, context.conversationId, message, function (err, response) {
if (err)
  console.log(err);
});
  res.status(200).end()
  //Use context to track your installs in your persistent data store.
})

router.post('/uninstalled', (req, res, next) => {
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  res.status(200).end()
  //Use context to track who uninstalls and remove them from your data store
})

module.exports = router
